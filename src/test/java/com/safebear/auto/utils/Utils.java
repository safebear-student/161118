package com.safebear.auto.utils;


import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {
    private static final String URL = System.getProperty("url","http://localhost:8080/");
    private static final String BROWSER = System.getProperty("browser","chrome");

    public static String getUrl(){
        return URL;
    }

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);

            case "headless":
                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);

            default:
                return new ChromeDriver(options);
        }

    }
    // We return our csv file as a `list of lists`.
    public static List<List<String>> getCsvData(String filename){
        // Import the `java.io` library for the `File` class
        File file = new File(filename);
        /**
         * Here we're setting up a list of lists.
         * The first list will contain the rows and the second will contain the data
         in the row
         * i.e. each row is a list of data, and we need a list of rows.
         * We know that the data in our table is Strings rather than Integers. However
         this might not always be the case.
         */
        List<List<String>> rows = new ArrayList<>();
        // We need a 'scanner' to import the file. Use the `java.Util` library
        Scanner inputStream;
        try{
            // Open the file
            inputStream = new Scanner(file);

            while (inputStream.hasNext()){
                // Each row is imported as single String
                String row = inputStream.next();
                // We need to break each row into data, we'll use the `comma` to identify where split it up
                String[] data = row.split(",");
                // Now add this data to our List of Lists.
                rows.add(Arrays.asList(data));
            }
            // Close the file
            inputStream.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
        // Return our list of rows
        return rows;
    }

    public static void captureScreenshot(WebDriver driver, String fileName) {
        // Take the screenshot with your `driver`
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs
                (OutputType.FILE);
        // Copy file to `screenshots` using the file name we pass through.
        try {
            copy(scrFile, new File("screenshots/" + fileName ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
