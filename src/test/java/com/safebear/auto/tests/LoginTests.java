package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class LoginTests extends BaseTest {

    private SoftAssert softly = new SoftAssert();

    Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
    Date date = new Date();

    @Test
    public void loginTest(){

        // Step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getUrl());
        // Step 1 EXPECTED RESULT: check we're on the Login Page
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page" , "The Login Page didn't open, or the title text has changed");
        // Step 2 ACTION: Enter Username and Password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        // Step 3 ACTION: Press the Login button
        loginPage.clickLoginButton();
        // Step 3 EXPECTED RESULT: check that we're now on the Tools Page
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "The Tools Page didn't open, or the title text has changed");
        Utils.captureScreenshot(driver, "loggedIn"+ formatter.format(date) +".png");


//        List<List<String>> rows = Utils.getCsvData("C:\\Users\\cca_student\\IdeaProjects\\161118\\src\\test\\resources\\users.csv");
//        // For each 'row' in 'rows', create a list containing the username and password. The 'for' command will loop for every row in the table.
//        for (List<String> row : rows ) {
//            // Step 1 ACTION: Open webpage
//            driver.get(Utils.getUrl());
//            // Step 1 EXPECTED RESULT: check we're on the Login Page
//            Assert.assertEquals(loginPage.getPageTitle(), "Login Page" , "The Login Page didn't open, or the title text has changed");
//            // Step 2 ACTION: Enter Username and Password
//            // Get the data in the first (index of 0) column - which is the Username
//            loginPage.enterUsername(row.get(0));
//            // Get the data in the second column - which is the password*
//            loginPage.enterPassword(row.get(1));
//            // Step 3 ACTION: Press the Login button
//            loginPage.clickLoginButton();
//            // Step 3 EXPECTED RESULT: check that we're now on the Tools Page
//            Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "The Login Page didn't open, or the title text has changed");

        }

    }
