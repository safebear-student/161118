package pages;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;
import pages.locators.ToolsPageLocators;

@RequiredArgsConstructor
public class ToolsPage {
    ToolsPageLocators locators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle(){
        return driver.getTitle().toString();
    }
}
